import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations([
  //   DeviceOrientation.landscapeLeft
  // ]);
  // Future.delayed(const Duration(seconds: 5), () {
  //   SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  // });
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: FutureBuilder(
          future: FlutterBluetoothSerial.instance.requestEnable(),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data.toString() == "true") {
              // Navigator.push(context, MaterialPageRoute(builder: (context) {return const Chat(server: null); }));
              return BluetoothApp();
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }
        )
    );
  }
}

class BluetoothApp extends StatefulWidget {
  @override
  _BluetoothAppState createState() => _BluetoothAppState();
}

class _BluetoothAppState extends State<BluetoothApp> {
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;

  FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;

  BluetoothConnection? connection;

  bool get isConnected => connection != null && connection!.isConnected;

  int _deviceState = 0;

  List<BluetoothDevice> _devicesList = [];

  bool isDisconnecting = false;

  bool _connected = false;

  @override
  void initState() {
    super.initState();

    FlutterBluetoothSerial.instance.state.then((state) {
      setState(() {
        _bluetoothState = state;
      }); 
    });

    _deviceState = 0;

    getPairedDevices();

    FlutterBluetoothSerial.instance.onStateChanged().listen((BluetoothState state) {
      setState(() {
        _bluetoothState = state;

        getPairedDevices();
      });
    });
  }

  Future<void> getPairedDevices() async {
    List<BluetoothDevice> devices = [];

    try {
      devices = await _bluetooth.getBondedDevices();
    } on PlatformException {
      print("Error");
    }

    if (!mounted) {
      return;
    }

    setState(() {
      _devicesList = devices;
    });
  }

  @override
  void dispose() {
    if (isConnected) {
      isDisconnecting = true;
      connection?.dispose();
      connection = null;
    }

    super.dispose();
  }

  void connectToDevice(BluetoothDevice device) async {
    if (device == null) {
      print('No device selected');
    } else {
      if (!isConnected) {
        await BluetoothConnection.toAddress(device.address).then((_connection) {
          print('Connected to the device');
          connection = _connection;

          setState(() {
            _connected = true;
          });
        }).catchError((error) {
          print('Cannot connect, exception occured');
          print(error);
        });
      } else {
        print('Device already connected!');
      }

    }
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Bluetooth test App')),
        body: _connected ? Chat(connection) : ListView.builder(
          itemCount: _devicesList.length,
          padding: const EdgeInsets.only(top: 10, bottom: 70),
          itemBuilder: (context, index) {
            return ListTile(
              title: Text("${_devicesList[index].name}"),
              onTap: () {connectToDevice(_devicesList[index]);},
            );
          }
        )
      ),
    );
  }
}


class ChatMessage{
  String message;
  bool userSent;

  ChatMessage({required this.message, required this.userSent});
}

class Chat extends StatefulWidget {
  BluetoothConnection? connection;

  Chat(this.connection);

  @override
  State<Chat> createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  List<ChatMessage> messages = [];

  String message = "";

  BluetoothConnection? connection;

  final ScrollController _controller = ScrollController();  // for list scroll

  @override
  void initState() {
    super.initState();
    connection = widget.connection;

    connection!.input!.listen((event) {
      addMessage(ascii.decode(event), false);
      _scrollDown();
    }).onDone(() {print("Connection done");});
  }

  void _SendMessageToBluetooth() {
    connection!.output.add(ascii.encode(message));

    connection?.output.allSent.then((value) {
      print("Message sent!");
      addMessage(message, true);
    }).onError((error, stackTrace) {
      print("Error sending message");
    });
  }

  addMessage(String msg, bool admin) {
    setState(() {
      messages.add(ChatMessage(message: msg, userSent: admin));

      _scrollDown();
    });
  }

  void _scrollDown() {
  _controller.animateTo(
    _controller.position.maxScrollExtent,
    duration: Duration(seconds: 1),
    curve: Curves.fastOutSlowIn,
  );
}

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
            controller: _controller,
            itemCount: messages.length,
            padding: const EdgeInsets.only(top: 10, bottom: 80),
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  shape: RoundedRectangleBorder(
                    side: const BorderSide(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(5)
                  ),
                  leading: messages[index].userSent ? const Icon(Icons.person) : Text(messages[index].message),
                  trailing: messages[index].userSent ? Text(messages[index].message) : const Icon(Icons.bluetooth),
                ),
              );
            }
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: const EdgeInsets.only(left: 10,bottom: 10,top: 10),
            height: 60,
            width: double.infinity,
            color: Colors.white,
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    onTap: () {Future.delayed(const Duration(milliseconds: 250), () {_scrollDown();});}, 
                    onChanged: (text) => setState(() {
                      message = text;
                    }),
                    decoration: const InputDecoration(
                      hintText: "Write message...",
                      hintStyle: TextStyle(color: Colors.black54),
                      border: InputBorder.none
                    ),
                  ),
                ),
                SizedBox(width: 15,),
                FloatingActionButton(
                  onPressed: (){_SendMessageToBluetooth();},
                  child: Icon(Icons.send,color: Colors.white,size: 18,),
                  backgroundColor: Colors.blue,
                  elevation: 0,
                ),
              ],
            )
          )
        )
      ],
    );
  }
}





// TAblet Test

// class _MyAppState extends State<MyApp> {
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     double height = size.height;
//     double width = size.width;

//     // return MaterialApp(
//       return  Scaffold(
//         body: Column(
//           children: [
//             Row(
//               children: [
//                 Container(
//                   width: 350,
//                   height: 120,
//                   color: Colors.blue,
//                   child: const Icon(Icons.forest),
//                 ),
//               ]
//             ),
//             Expanded(
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 children: [
//                   Padding(
//                     padding: EdgeInsets.all(15),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.end,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         InkWell(
//                           onTap: () => Navigator.of(context).push(
//                             PageRouteBuilder(pageBuilder: ((context, animation, secondaryAnimation) => Scaffold(body: NewPageTest(['$height', '$width']))),
//                             transitionsBuilder: (context, animation, secondaryAnimation, child) {
//                               const begin = Offset(0.0, 1.0);
//                               const end = Offset.zero;
//                               const curve = Curves.ease;

//                               final tween = Tween(begin: begin, end: end);
//                               final curvedAnimation = CurvedAnimation(
//                                 parent: animation,
//                                 curve: curve,
//                               );

//                               return SlideTransition(
//                                 position: tween.animate(curvedAnimation),
//                                 child: child,
//                               );
//                             }
//                           )),
//                           child: Container(
//                             width: 300,
//                             height: 150,
//                             color: Colors.purple,
//                             child: Center(
//                               child: Text("$height x $width", style: const TextStyle(fontSize: 35),
//                             )),
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.only(top: 15),
//                           child: InkWell(
//                             onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Scaffold(body: FutureBuilderTest()))),
//                             child: Container(
//                               width: 350,
//                               height: 250,
//                               color: Colors.red,
//                               child: const Center(child: Text("FUTURE", style: TextStyle(fontSize: 35))),
//                             ),
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//                   Expanded(
//                     child: Center(
//                       child: Container(
//                         width: 400,
//                         height: 400,
//                         color: Colors.green
//                       )
//                     ),
//                   ),
//                   Padding(
//                     padding: EdgeInsets.all(15),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: [
//                         Container(
//                           width: 300,
//                           height: 150,
//                           color: Color.fromARGB(255, 177, 164, 52),
//                           child: InkWell(
//                             onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Scaffold(body: FutureBuilderTest()))),
//                             child: Container(
//                               width: 350,
//                               height: 250,
//                               color: Colors.red,
//                               child: const Center(child: Text("FUTURE", style: TextStyle(fontSize: 35))),
//                             ),
//                           ),
//                         ),
//                         Container(
//                           width: 300,
//                           height: 150,
//                           color: Color.fromARGB(255, 230, 218, 112)
//                         ),
//                         Container(
//                           width: 300,
//                           height: 150,
//                           color: Colors.yellow
//                         )
//                       ],
//                     ),
//                   )
//                 ],
//               ),
//             )
//           ],

//           // child: Column(
//           //   mainAxisAlignment: MainAxisAlignment.center,
//           //   children: [
//           //     Text(
//           //       "Height $height",
//           //       style: const TextStyle(fontSize: 60),
//           //     ),
//           //     Text(
//           //       "Width $width",
//           //       style: const TextStyle(fontSize: 60),
//           //     ),
//           //   ]
//           // ),

//           // Container(
//           //   width: size,
//           //   height: size,
//           //   color: color:
//           //   child: Widget
//           // )
//         )
//       );
//     // );
//   }
// }

// class NewPageTest extends StatelessWidget {
//   final List<String> texts;

//   const NewPageTest(this.texts);

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           for (var text in texts)
//             Text(text, style: const TextStyle(fontSize: 35))
//         ],
//       ),
//     );
//   }
// }

// Future<String> testFuture() {
//   return Future<String>.delayed(const Duration(seconds: 5), () async => 'Future completed');
// }

// class FutureBuilderTest extends StatefulWidget {
//   const FutureBuilderTest({Key? key}) : super(key: key);

//   @override
//   State<FutureBuilderTest> createState() => _FutureBuilderTestState();
// }

// class _FutureBuilderTestState extends State<FutureBuilderTest> {
//   Future<String>? delivery;

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: [
//           ElevatedButton(
//             onPressed: delivery != null ? null : () => setState(() { delivery = testFuture(); }),
//             child: const Text('Press me', style: TextStyle(fontSize: 18))
//           ),
//           delivery == null
//             ? const Text('Placeholder until we get data', style: TextStyle(fontSize: 35))
//             : FutureBuilder(
//               future: delivery,
//               builder: (context, snapshot) {
//                 if(snapshot.hasData) {
//                   return Text('${snapshot.data}', style: const TextStyle(fontSize: 35));
//                 } else if(snapshot.hasError) {
//                   return Text(snapshot.error.toString(), style: const TextStyle(fontSize: 35));
//                 } else {
//                   return const CircularProgressIndicator();
//                 }
//               })
//         ]),
//     );
//   }
// }

// class BluetoothTest extends StatefulWidget {
//   const BluetoothTest({Key? key}) : super(key: key);

//   @override
//   State<BluetoothTest> createState() => _BluetoothTestState();
// }

// class _BluetoothTestState extends State<BluetoothTest> {
//   @override
//   Widget build(BuildContext context) {
    
//   }
// }